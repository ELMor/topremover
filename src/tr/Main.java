package tr;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Main {
	public static void main(String args[]){
		File f=new File(args[0]);
		recurse(f);
	}
	public static void recurse(File d){
		File list[]=d.listFiles();
		for(File f:list){
			if(f.isDirectory())
				recurse(f);
			else if(f.getName().toLowerCase().endsWith(".epub")){
				checkTop(f);
			}
		}
	}
	public static void checkTop(File f){
		try{
			System.out.print("checking "+f.getName());
			ZipInputStream zis=new ZipInputStream(new FileInputStream(f));
			ZipEntry ze;
			boolean mustClose=true;
			while((ze=zis.getNextEntry())!=null){
				if(ze.getName().toLowerCase().endsWith("toc.ncx")){
					ByteArrayOutputStream baos=getbaos(zis);
					String cnt=new String(baos.toByteArray());
					if(cnt.indexOf(".xhtml#top")>=0){
						System.out.print(" removing top");
						zis.close();
						mustClose=false;
						removeTop(f);
						System.out.print(" removed\n");
						break;
					}
				}
			}
			if(mustClose){
				zis.close();
				System.out.print(" no #top\n");
			}
				
		}catch(IOException ioe){
			
		}
	}
	
	public static void removeTop(File f){
		try {
			String wfn=f.getCanonicalPath();
			File g=new File(wfn+"-tmp");
			f.renameTo(g);
			ZipEntry ze;
			ze = new ZipEntry("mimetype");
			ze.setMethod(ZipEntry.STORED);
			ze.setSize(20);
			ze.setCrc(0x2cab616f);
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(wfn));
			zos.putNextEntry(ze);
			zos.write("application/epub+zip".getBytes());
			ZipInputStream zis=new ZipInputStream(new FileInputStream(g));
			while((ze=zis.getNextEntry())!=null){
				String zen=ze.getName().toLowerCase();
				if(zen.endsWith("mimetype"))
					continue;
				else if(zen.endsWith("toc.ncx")){
					ByteArrayOutputStream baos=getbaos(zis);
					String c=new String(baos.toByteArray());
					c=c.replace(".xhtml#top", ".xhtml");
					zos.putNextEntry(new ZipEntry(ze.getName()));
					zos.write(c.getBytes());
				}else {
					ByteArrayOutputStream baos=getbaos(zis);
					zos.putNextEntry(ze);
					zos.write(baos.toByteArray());
				}
			}
			zos.close();
			zis.close();
			g.delete();
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
	}

	public static ByteArrayOutputStream getbaos(ZipInputStream zis) throws IOException{
		byte buf[]=new byte[1024];
		int len=0;
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		while((len=zis.read(buf))!=-1){
			baos.write(buf, 0, len);
		}
		return baos;
	}
	
}
